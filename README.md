# Automatic Tor NGINX

Automatic Tor+Nginx is a bash script that automatically installs Tor and NGINX and automatically configures them to create a hidden website accessible through the Tor network.

## System Requirements:

Debian 10 (only tested with 64bit).

## Installation instructions: 

### Download the bash script:
wget https://gitlab.com/worldofmatthew/automatic-tor-nginx/raw/master/tornginx.sh
### set the correct permissions:
chmod +x tornginx.sh
### Run the bash script:
./tornginx.sh

After installation is complete, you will be provided both your onion v3 hostname and the WWW directory that NGINX in configured to use.


